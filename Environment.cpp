#include "Utility.cpp"

class Sensor {
    protected:
        string inputFileName = "data.csv";
        float value;

    public:
        Sensor() { value = 0;}
        Sensor(float value) {
            value = value;
        }

        ~Sensor() {}

        float getValue() const {
            return this->value;
        }

        virtual float getCurrentValue() const = 0;
        virtual void setCurrentValue(float value) = 0;

        string getInputFName() const {
            return this->inputFileName;
        }

        void setInputFName(string newFileName) {
            this->inputFileName = newFileName;
        }
};


class TemperatureSensor : public Sensor
{
    public:
        TemperatureSensor() {}
        TemperatureSensor(float temperature) : Sensor(temperature){}
        ~TemperatureSensor() {}

        float getTemperature(vector<string> row) const {
            return stof(row[0]);
        }

        float getCurrentValue() const {
            float currentTemperature;
            vector<string> record = readRecord(getInputFName());
            if(record.empty())
            {
                vector<string> record = readRecord(getInputFName());
            }
            currentTemperature = getTemperature(record);

            return currentTemperature;
        }

        void setCurrentValue(float currentTemp) {
            this->value = currentTemp;
        }
};


class HumiditySensor : public Sensor
{
    public:
        HumiditySensor() {}
        HumiditySensor(float humidity) : Sensor(humidity) {}
        ~HumiditySensor() {}

        float getHumidity(vector<string> row) const
        {
            return stof(row[1]);
        }

        float getCurrentValue() const {
            float currentHumidity;
            vector<string> record = readRecord(getInputFName());
            if(record.empty())
            {
                vector<string> record = readRecord(getInputFName());
            }
            currentHumidity = getHumidity(record);

            return currentHumidity;
        }

        void setCurrentValue(float currentHum) {
            this->value = currentHum;
        }

};

/*
    Defining the Environment class. It models the entire configuration of the Environment.
*/
class Environment {
    public:
        Environment() {}
        ~Environment() {}

        string get(string type) {
            if(type == "temperature") {
                return to_string(temperatureSensor.getValue());
            }else if (type == "humidity")
            {
                return to_string(humiditySensor.getValue());
            }
            return "";            
        }

        void set(string type, float value)  {
            if(type == "temperature") {
                temperatureSensor.setCurrentValue(value);
            }else if(type == "humidity") {
                humiditySensor.setCurrentValue(value);
            }

            cout << "Sensor type inexistent";
        }

    private:
        TemperatureSensor temperatureSensor;
        HumiditySensor humiditySensor;

        void setTemperature() {
            temperatureSensor.setCurrentValue(temperatureSensor.getCurrentValue());
        }

        void setTemperature(float temperature) {
            temperatureSensor.setCurrentValue(temperature);
        }

        void setHumidity() {
            humiditySensor.setCurrentValue(humiditySensor.getCurrentValue());
        }

        void setHumidity(float humidity) {
            humiditySensor.setCurrentValue(humidity);
        }
};


// // Definition of the EnvironmentEndpoint class
// class EnvironmentEndpoint {
//     public:
//         explicit EnvironmentEndpoint(Address addr) : httpEndpoint(std::make_shared<Http::Endpoint>(addr)) {}

//         // Initialization of the server. Additional options can be provided here
//         void init(size_t thr = 2)
//         {
//             auto opts = Http::Endpoint::options().threads(static_cast<int>(thr));
//             httpEndpoint->init(opts);
//             // Server routes are loaded up
//             setupRoutes();
//         }

//         // Server is started threaded.
//         void start()
//         {
//             httpEndpoint->setHandler(router.handler());
//             httpEndpoint->serveThreaded();
//         }

//         // When signaled server shuts down
//         void stop()
//         {
//             cout << "Shutting down..." << endl;
//             httpEndpoint->shutdown();
//         }

//     private:

//         // Create the lock which prevents concurrent editing of the same variable 
//         using Lock = std::mutex;
//         using Guard = std::lock_guard<Lock>;
//         Lock environmentLock;

//         // Instance of the Environment model
//         Environment env;

//         // Defining the httpEndpoint and a router.
//         std::shared_ptr<Http::Endpoint> httpEndpoint;
//         Rest::Router router;

//         void setupRoutes()
//         {
//             using namespace Rest;
//             Routes::Get(router, "/environment/sensorValue/:sensorType", Routes::bind(&EnvironmentEndpoint::getSettings, this));
//         }

//         void getSettings(const Rest::Request& request, Http::ResponseWriter response) {
//             auto sensorType = request.param(":sensorType").as<std::string>();

//             Guard guard(environmentLock);
//             string valueSetting = env.get(sensorType);

//             if (valueSetting != "") {

//                 // In this response I also add a couple of headers, describing the server that sent this response, and the way the content is formatted.
//                 using namespace Http;
//                 response.headers()
//                     .add<Header::Server>("pistache/0.1")
//                     .add<Header::ContentType>(MIME(Text, Plain));

//                 response.send(Http::Code::Ok, sensorType + " is " + valueSetting);
//             }
//             else {
//                 response.send(Http::Code::Not_Found, sensorType + " was not found");
//             }
//         }
// };
