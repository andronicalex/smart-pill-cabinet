#include "alertEndpoint.cpp"

// Definition of the UserEndpoint class
class UserEndpoint
{
public:
    explicit UserEndpoint(Address addr) : httpEndpoint(std::make_shared<Http::Endpoint>(addr)) {}

    // Initialization of the server. Additional options can be provided here
    void init(size_t thr = 2)
    {
        auto opts = Http::Endpoint::options().threads(static_cast<int>(thr));
        httpEndpoint->init(opts);
        // Server routes are loaded up
        setupRoutes();
    }

    // Server is started threaded.
    void start()
    {
        httpEndpoint->setHandler(router.handler());
        httpEndpoint->serveThreaded();
    }

    // When signaled server shuts down
    void stop()
    {
        cout << "Shutting down..." << endl;
        httpEndpoint->shutdown();
    }

private:
    // Create the lock which prevents concurrent editing of the same variable
    using Lock = std::mutex;
    using Guard = std::lock_guard<Lock>;
    Lock UserLock;

    // Defining the httpEndpoint and a router.
    std::shared_ptr<Http::Endpoint> httpEndpoint;
    Rest::Router router;

    void setupRoutes()
    {
        using namespace Rest;
        Routes::Get(router, "/user/check/:userId", Routes::bind(&UserEndpoint::checkUser, this));

        Routes::Get(router, "/user/all", Routes::bind(&UserEndpoint::getUsers, this));
        Routes::Get(router, "/user/current", Routes::bind(&UserEndpoint::getCurrentUser, this));
        Routes::Post(router, "/user/current/:userId", Routes::bind(&UserEndpoint::setCurrentUser, this));

        Routes::Post(router, "/user/:userName/:userAge", Routes::bind(&UserEndpoint::setUser, this));
        Routes::Patch(router, "/user/:userId/:userName/:userAge", Routes::bind(&UserEndpoint::editUser, this));
        Routes::Delete(router, "/user/:userId", Routes::bind(&UserEndpoint::deleteUser, this));

        Routes::Get(router, "/user/prescription/all", Routes::bind(&UserEndpoint::getAllPrescriptions, this));
        Routes::Get(router, "/user/prescription/:userId", Routes::bind(&UserEndpoint::getPrescriptionsForUser, this));
        Routes::Post(router, "/user/prescription/:drugName/:quantity", Routes::bind(&UserEndpoint::setPrescription, this));
        Routes::Patch(router, "/user/prescription/:prescriptionId/:drugName/:quantity", Routes::bind(&UserEndpoint::editPrescription, this));
        Routes::Delete(router, "/user/prescription/:prescriptionId", Routes::bind(&UserEndpoint::deletePrescription, this));
    }

    void checkUser(const Rest::Request &request, Http::ResponseWriter response)
    {
        auto userId = request.param(":userId").as<std::string>();
        Guard guard(UserLock);

        bool valueSetting = UserNamespace::checkUserExists(userId);

        if (valueSetting == true)
        {
            // In this response I also add a couple of headers, describing the server that sent this response, and the way the content is formatted.
            using namespace Http;
            response.headers()
                .add<Header::Server>("pistache/0.1")
                .add<Header::ContentType>(MIME(Text, Plain));

            response.send(Http::Code::Ok, "True");
        }
        else
        {
            response.send(Http::Code::Not_Found, "False");
        }
    }

    void getUsers(const Rest::Request &request, Http::ResponseWriter response)
    {
        Guard guard(UserLock);

        string valueSetting = UserNamespace::getUsers();

        if (valueSetting != "")
        {
            // In this response I also add a couple of headers, describing the server that sent this response, and the way the content is formatted.
            using namespace Http;
            response.headers()
                .add<Header::Server>("pistache/0.1")
                .add<Header::ContentType>(MIME(Text, Plain));

            response.send(Http::Code::Ok, "Users:\n" + valueSetting);
        }
        else
        {
            response.send(Http::Code::Not_Found, "No users yet.\n");
        }
    }

    void getCurrentUser(const Rest::Request &request, Http::ResponseWriter response)
    {
        Guard guard(UserLock);

        string valueSetting = UserNamespace::getCurrentUser();

        if (valueSetting != "")
        {
            // In this response I also add a couple of headers, describing the server that sent this response, and the way the content is formatted.
            using namespace Http;
            response.headers()
                .add<Header::Server>("pistache/0.1")
                .add<Header::ContentType>(MIME(Text, Plain));

            response.send(Http::Code::Ok, "Current user:\n" + valueSetting);
        }
        else
        {
            response.send(Http::Code::Not_Found, "No users yet.\n");
        }
    }

    void setUser(const Rest::Request &request, Http::ResponseWriter response)
    {
        auto userName = request.param(":userName").as<std::string>();
        auto userAge = request.param(":userAge").as<std::string>();

        Guard guard(UserLock);

        int userId = UserNamespace::writeUser(userName, userAge);
        
        if (userId != 0)
        {   
            int addResponse = AlertEndpoint::addAlertManager(userId);

            if(addResponse != 0){
                response.send(Http::Code::Bad_Request, "Not good!\n");
            }
            response.send(Http::Code::Ok, "User added!\n");
        }
        else
        {
            response.send(Http::Code::Not_Found, "Failed to create user!\n");
        }
    }

    void setCurrentUser(const Rest::Request &request, Http::ResponseWriter response)
    {
        auto userId = request.param(":userId").as<std::string>();

        Guard guard(UserLock);

        int setResponse = UserNamespace::setCurrentUser(userId);

        if (setResponse == 1)
        {
            response.send(Http::Code::Ok, "Successfully set current user!\n");
        }
        else
        {
            response.send(Http::Code::Not_Found, "Failed to set current user!\n");
        }
    }

    void editUser(const Rest::Request &request, Http::ResponseWriter response)
    {
        auto userId = request.param(":userId").as<std::string>();
        auto userName = request.param(":userName").as<std::string>();
        auto userAge = request.param(":userAge").as<std::string>();

        Guard guard(UserLock);

        int setResponse = UserNamespace::editUser(userId, userName, userAge);

        if (setResponse == 1)
        {
            response.send(Http::Code::Ok, "User edited successfully!\n");
        }
        else
        {
            response.send(Http::Code::Not_Found, "Failed to edit user!\n");
        }
    }

    void deleteUser(const Rest::Request &request, Http::ResponseWriter response)
    {
        auto userId = request.param(":userId").as<std::string>();

        Guard guard(UserLock);

        int setResponse = UserNamespace::deleteUser(userId);
    
        if (setResponse == 1)
        {
            int deleteAlertManager = AlertEndpoint::deleteAlertManager(stoi(userId));
            if(deleteAlertManager == 0)
                response.send(Http::Code::Ok, "User deleted successfully!\n");
            else
                response.send(Http::Code::Not_Found, "Failed to delete user!\n");
        }
        else
        {
            response.send(Http::Code::Not_Found, "Failed to delete user!\n");
        }
    }

    void getAllPrescriptions(const Rest::Request &request, Http::ResponseWriter response)
    {
        Guard guard(UserLock);

        string valueSetting = UserNamespace::getAllPrescriptions();

        if (valueSetting != "")
        {
            // In this response I also add a couple of headers, describing the server that sent this response, and the way the content is formatted.
            using namespace Http;
            response.headers()
                .add<Header::Server>("pistache/0.1")
                .add<Header::ContentType>(MIME(Text, Plain));

            response.send(Http::Code::Ok, "Prescriptions:\n" + valueSetting);
        }
        else
        {
            response.send(Http::Code::Not_Found, "No prescriptions yet.\n");
        }
    }

    void getPrescriptionsForUser(const Rest::Request &request, Http::ResponseWriter response)
    {
        auto userId = request.param(":userId").as<std::string>();
        Guard guard(UserLock);

        string valueSetting = UserNamespace::getPrescriptionsForUser(userId);

        if (valueSetting != "")
        {
            // In this response I also add a couple of headers, describing the server that sent this response, and the way the content is formatted.
            using namespace Http;
            response.headers()
                .add<Header::Server>("pistache/0.1")
                .add<Header::ContentType>(MIME(Text, Plain));

            response.send(Http::Code::Ok, "Prescriptions:\n" + valueSetting);
        }
        else
        {
            response.send(Http::Code::Not_Found, "No prescriptions yet.");
        }
    }

    void setPrescription(const Rest::Request &request, Http::ResponseWriter response)
    {
        auto drugName = request.param(":drugName").as<std::string>();
        auto quantity = request.param(":quantity").as<std::string>();

        Guard guard(UserLock);

        int setResponse = UserNamespace::setPrescription(drugName, quantity);

        if (setResponse == 1)
        {
            response.send(Http::Code::Ok, "Prescription added successfully!\n");
        }
        else
        {
            response.send(Http::Code::Not_Found, "Failed to add prescription!\n");
        }
    }

    void editPrescription(const Rest::Request &request, Http::ResponseWriter response)
    {
        auto prescriptionId = request.param(":prescriptionId").as<std::string>();
        auto drugName = request.param(":drugName").as<std::string>();
        auto quantity = request.param(":quantity").as<std::string>();

        Guard guard(UserLock);

        int setResponse = UserNamespace::editPrescription(prescriptionId, drugName, quantity);

        if (setResponse == 1)
        {
            response.send(Http::Code::Ok, "Prescription edited successfully!\n");
        }
        else
        {
            response.send(Http::Code::Not_Found, "Failed to edit prescription!\n");
        }
    }

    void deletePrescription(const Rest::Request &request, Http::ResponseWriter response)
    {
        auto prescriptionId = request.param(":prescriptionId").as<std::string>();

        Guard guard(UserLock);

        int setResponse = UserNamespace::deletePrescription(prescriptionId);

        if (setResponse == 1)
        {
            response.send(Http::Code::Ok, "Prescription deleted successfully!\n");
        }
        else
        {
            response.send(Http::Code::Not_Found, "Failed to delete prescription!\n");
        }
    }
};