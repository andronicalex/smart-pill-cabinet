#include "helpers.cpp"

namespace UserNamespace
{
    string getUsers()
    {
        string result = "";

        fstream fin;
        fin.open("users.csv", ios::in);

        int index = 0;
        for (std::string line; getline(fin, line);)
        {
            if (index > 0)
                result += "\n";
            result = result + line;
            index += 1;
        }

        fin.close();
        return result;
    }

    bool checkUserExists(string userId)
    {
        fstream fin;
        fin.open("users.csv", ios::in);

        int userIdCSV;
        for (std::string line; getline(fin, line);)
        {
            std::stringstream ss(line);
            ss >> userIdCSV;

            if (userIdCSV == stoi(userId))
                return true;
        }

        fin.close();
        return false;
    }

    string getCurrentUser()
    {
        fstream fin;
        fin.open("users.csv", ios::in);

        for (std::string line; getline(fin, line);)
        {
            if (line.find('*') != std::string::npos)
                return line;
        }

        fin.close();
        return "";
    }

    int getLastUserIndex()
    {
        int i = 0;
        std::string line;

        fstream fin;
        fin.open("users.csv", ios::in);

        while (getline(fin, line))
        {
            std::stringstream ss(line);
            ss >> i;
        }

        fin.close();
        return i;
    }

    int writeUser(string userName, string userAge)
    {
        try
        {
            fstream fout;
            fout.open("users.csv", ios::out | ios::app);
            int userId = getLastUserIndex();

            if (userId == 0)
                fout << userId + 1 << "," << userName << "," << userAge << ",*";
            else
                fout << "\n"
                     << userId + 1 << "," << userName << "," << userAge << ",-";
            fout.close();
            return userId + 1;
        }
        catch (...)
        {
            return 0;
        }
    }

    bool editUser(string userId, string userName, string userAge)
    {
        try
        {
            fstream fin, faux;
            fin.open("users.csv", ios::in);
            faux.open("usersAux.csv", ios::out);

            int index = 0;
            for (std::string line; getline(fin, line);)
            {
                index = index + 1;
                std::vector<int> vect;
                std::stringstream ss(line);
                int i;
                ss >> i;
                if (index > 1)
                    faux << "\n";

                if (i == stoi(userId))
                    faux << userId << "," << userName << "," << userAge;
                else
                    faux << line;
            }

            fin.close();
            faux.close();

            // removing the existing file
            remove("users.csv");

            // renaming the updated file with the existing file name
            rename("usersAux.csv", "users.csv");

            return 1;
        }
        catch (...)
        {
            return 0;
        }
    }

    bool setCurrentUser(string userId)
    {
        try
        {
            fstream fin, faux;
            fin.open("users.csv", ios::in);
            faux.open("usersAux.csv", ios::out);

            int index = 0, userIdCVS;
            for (std::string line; getline(fin, line);)
            {
                index += 1;

                if (line.find('*') != std::string::npos)
                    std::replace(line.begin(), line.end(), '*', '-');

                std::stringstream ss(line);
                ss >> userIdCVS;

                if (stoi(userId) == userIdCVS)
                    std::replace(line.begin(), line.end(), '-', '*');

                if (index > 1)
                    faux << "\n";
                faux << line;
            }

            fin.close();
            faux.close();

            // removing the existing file
            remove("users.csv");

            // renaming the updated file with the existing file name
            rename("usersAux.csv", "users.csv");

            return 1;
        }
        catch (...)
        {
            return 0;
        }
    }

    bool deleteUser(string userId)
    {
        try
        {
            fstream fin, faux;
            fin.open("users.csv", ios::in);
            faux.open("usersAux.csv", ios::out);

            int index = 0;
            for (std::string line; getline(fin, line);)
            {
                std::vector<int> vect;
                std::stringstream ss(line);
                int i;
                ss >> i;

                if (i != stoi(userId))
                {
                    if (index > 0)
                        faux << "\n";

                    faux << line;
                    index = index + 1;
                }
            }

            fin.close();
            faux.close();

            // removing the existing file
            remove("users.csv");

            // renaming the updated file with the existing file name
            rename("usersAux.csv", "users.csv");

            return 1;
        }
        catch (...)
        {
            return 0;
        }
    }

    string getAllPrescriptions()
    {
        string result = "";

        fstream fin;
        fin.open("prescription.csv", ios::in);

        int index = 0;
        for (std::string line; getline(fin, line);)
        {
            if (index > 0)
                result += "\n";
            result = result + line;
            index += 1;
        }

        fin.close();
        return result;
    }

    string getPrescriptionsForUser(string userId)
    {
        string result = "";

        fstream fin;
        fin.open("prescription.csv", ios::in);

        int index = 0, userIdCSV, prescriptionId;
        for (std::string line; getline(fin, line);)
        {
            std::stringstream ss(line);
            ss >> prescriptionId;
            if (ss.peek() == ',')
                ss.ignore();
            ss >> userIdCSV;

            if (userIdCSV == stoi(userId))
            {
                if (index > 0)
                    result += "\n";

                result = result + line;
                index += 1;
            }
        }

        fin.close();
        return result;
    }

    int getLastPrescriptionIndex()
    {
        int i = 0;
        std::string line;

        fstream fin;
        fin.open("prescription.csv", ios::in);

        while (getline(fin, line))
        {
            std::stringstream ss(line);
            ss >> i;
        }

        fin.close();
        return i;
    }

    bool setPrescription(string drugName, string quantity)
    {
        try
        {
            fstream fout;
            fout.open("prescription.csv", ios::out | ios::app);
            string currentUserEntry = getCurrentUser();
            int userId, lastPrescriptionId;

            std::stringstream ss(currentUserEntry);
            ss >> userId;

            lastPrescriptionId = getLastPrescriptionIndex();

            if (lastPrescriptionId > 0)
                fout << "\n";

            fout << lastPrescriptionId + 1 << "," << userId << "," << drugName << "," << quantity;

            fout.close();
            return 1;
        }
        catch (...)
        {
            return 0;
        }
    }

    bool editPrescription(string prescriptionId, string drugName, string quantity)
    {
        try
        {
            fstream fin, faux;
            fin.open("prescription.csv", ios::in);
            faux.open("prescriptionAux.csv", ios::out);

            int index = 0;
            for (std::string line; getline(fin, line);)
            {
                index = index + 1;
                std::vector<int> vect;
                std::stringstream ss(line);
                int prescriptionIdCSV, userId;
                ss >> prescriptionIdCSV;

                if (ss.peek() == ',')
                    ss.ignore();
                ss >> userId;

                if (index > 1)
                    faux << "\n";

                if (prescriptionIdCSV == stoi(prescriptionId))
                    faux << prescriptionId << "," << userId << "," << drugName << "," << quantity;
                else
                    faux << line;
            }

            fin.close();
            faux.close();

            // removing the existing file
            remove("prescription.csv");

            // renaming the updated file with the existing file name
            rename("prescriptionAux.csv", "prescription.csv");

            return 1;
        }
        catch (...)
        {
            return 0;
        }
    }

    bool deletePrescription(string prescriptionId)
    {
        try
        {
            fstream fin, faux;
            fin.open("prescription.csv", ios::in);
            faux.open("prescriptionAux.csv", ios::out);

            int index = 0;
            for (std::string line; getline(fin, line);)
            {
                std::vector<int> vect;
                std::stringstream ss(line);
                int i;
                ss >> i;

                if (i != stoi(prescriptionId))
                {
                    if (index > 0)
                        faux << "\n";

                    faux << line;
                    index = index + 1;
                }
            }

            fin.close();
            faux.close();

            // removing the existing file
            remove("prescription.csv");

            // renaming the updated file with the existing file name
            rename("prescriptionAux.csv", "prescription.csv");

            return 1;
        }
        catch (...)
        {
            return 0;
        }
    }

}