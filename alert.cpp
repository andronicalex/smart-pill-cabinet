#include<iostream>
#include<string>
#include<vector>

using namespace std;

class Alert{

    static int currentId; 
    int alertId;
    string pillId;
    string text;
    public:
        Alert(string id, string s);
        int getAlertId();
        string getPillId();
        string getText();
};

class AlertManager{

    int userId;
    bool active;
    bool reducedActive;
    bool expirationActive;
    vector<Alert> administrationAlerts;

    public:
        AlertManager(int id, bool active = true);
        int addAdmininistrationAlert(string pillId);
        int removeAdministrationAlert(string pillId);
        string getAdministrationAlerts();
        //void addReducedStockAlert(string pillId);
        //void removeReducedStockAlert(bool status);
        void setReducedStockStatus(bool status);
        //string getReducedStockAlerts();
        //void addExpirationAlert(string pillId);
        //void removeExpirationAlert(bool status);
        void setExpirationStatus(bool status);
        //string getExpirationAlerts();
        int getUserId();
        bool getReducedStockStatus();
        bool getExpirationStatus();
        bool isActive();
        string getTextAdm();
};
int Alert::currentId = 0;
Alert::Alert(string id, string s){
    currentId += 1;
    alertId = currentId;
    pillId = id;
    text = s;
}
int Alert::getAlertId(){
    return alertId;
}
string Alert::getPillId(){
    return pillId;
}
string Alert::getText(){
    return text;
}

AlertManager::AlertManager(int id, bool active){
            userId = id;
            this->active = active;
            this->expirationActive = active;
            this->reducedActive = active;
        }
int AlertManager::addAdmininistrationAlert(string pillId){
    
    if(active){
        try{
            for(int i = 0; i < administrationAlerts.size(); i++){
                if(administrationAlerts[i].getPillId() == pillId){
                    return 2;
                } 
            }
            Alert alert(pillId,"You must take your pill!");
            administrationAlerts.push_back(alert);
            // OK
            return 0;
        }
        catch(...){
            // There is an error
            return 1;
        }

    }
    // The manager is not active
    return 2;

}
int AlertManager::removeAdministrationAlert(string pillId){
    if(active){
        vector<Alert>::iterator i;
        for(i = administrationAlerts.begin();i<administrationAlerts.end();i++){
            if(i->getPillId() == pillId){
                try{
                    administrationAlerts.erase(i);
                    return 0;
                }
                catch(...){
                    return 1;
                }

            } 
        }
    }
    return 2;

}
string AlertManager::getAdministrationAlerts(){
    string s = "";
    vector<Alert>::iterator i;
    for(i=administrationAlerts.begin(); i<administrationAlerts.end(); i++){
        s = s + to_string(i->getAlertId()) + i->getPillId() + i->getText() + "\n";
    }
    return s;
}
// void AlertManager::addReducedStockAlert(string pillId){
//     if(active){
//         for(int i = 0; i < reducedStockAlerts.size(); i++){
//             if(reducedStockAlerts[i].getPillId() == pillId){
//                 return;
//             } 
//         }
//         Alert alert(pillId,"The stock is low!");
//         reducedStockAlerts.push_back(alert);
//     }
// }
// void AlertManager::removeReducedStockAlert(bool status){
//     // vector<Alert>::iterator i;
//     // for(i = reducedStockAlerts.begin(); i < reducedStockAlerts.end(); i++){
//     //     if(i->getPillId() == pillId){
//     //         reducedStockAlerts.erase(i);
//     //         return;
//     //     } 
//     // }
//     this->reducedActive = status;
// }
void AlertManager::setReducedStockStatus(bool status){
    this->reducedActive = status;
}
// string AlertManager::getReducedStockAlerts(){
//     string s = "";
//     vector<Alert>::iterator i;
//     for(i=reducedStockAlerts.begin(); i<reducedStockAlerts.end(); i++){
//         s += to_string(i->getAlertId()) + i->getPillId() + i->getText() + "\n";
//     }
//     return s;
// }
// void AlertManager::addExpirationAlert(string pillId){
//     if(active){
//         for(int i = 0; i < expirationAlerts.size(); i++){
//             if(expirationAlerts[i].getPillId() == pillId){
//                 return;
//             } 
//         }
//         Alert alert(pillId,"The pills are about to expire!");
//         expirationAlerts.push_back(alert);
//     }
// }
// void AlertManager::removeExpirationAlert(bool status){
//     // vector<Alert>::iterator i;
//     // for(i = expirationAlerts.begin(); i < expirationAlerts.end(); i++){
//     //     if(i->getPillId() == pillId){
//     //         expirationAlerts.erase(i);
//     //         return;
//     //     } 
//     // }
//     this->expirationActive = status;
// }
void AlertManager::setExpirationStatus(bool status){
    this->expirationActive = status;
}
// string AlertManager::getExpirationAlerts(){
//     string s = "";
//     vector<Alert>::iterator i;
//     for(i=expirationAlerts.begin(); i<expirationAlerts.end(); i++){
//         s += to_string(i->getAlertId()) + i->getPillId() + i->getText() + "\n";
//     }
//     return s;
// }
int AlertManager::getUserId(){
    return userId;
}
bool AlertManager::getReducedStockStatus(){
    return reducedActive; 
};
bool AlertManager::getExpirationStatus(){
    return expirationActive;
};
bool AlertManager::isActive(){
    return active;
}
string AlertManager::getTextAdm(){
    //return administrationAlerts.begin()->getText();
    return to_string(administrationAlerts.size());
};
