#include "userNamespace.cpp"
#include "drugNamespace.cpp"

#include "alert.cpp"


class AlertEndpoint
{
private:
    static vector<AlertManager*> alertManagers;

public:
    explicit AlertEndpoint(Address addr) : httpEndpoint(std::make_shared<Http::Endpoint>(addr)) {}

    // Initialization of the server. Additional options can be provided here
    void init(size_t thr = 2)
    {
        auto opts = Http::Endpoint::options().threads(static_cast<int>(thr));
        httpEndpoint->init(opts);
        // Server routes are loaded up
        setupRoutes();
    }

    // Server is started threaded.
    void start()
    {
        httpEndpoint->setHandler(router.handler());
        httpEndpoint->serveThreaded();
    }

    // When signaled server shuts down
    void stop()
    {
        cout << "Shutting down..." << endl;
        httpEndpoint->shutdown();
    }

private:
    // Create the lock which prevents concurrent editing of the same variable
    using Lock = std::mutex;
    using Guard = std::lock_guard<Lock>;
    Lock UserLock;

    // Defining the httpEndpoint and a router.
    std::shared_ptr<Http::Endpoint> httpEndpoint;
    Rest::Router router;

    void setupRoutes()
    {
        using namespace Rest;

        Routes::Get(router, "/alerts/all", Routes::bind(&AlertEndpoint::getAlerts, this));
        Routes::Get(router, "/alerts/user/:userId", Routes::bind(&AlertEndpoint::getAlertsFromUser, this));
        Routes::Post(router, "/alerts/administration/:userId/:pillId", Routes::bind(&AlertEndpoint::createAlertForUser, this));
        Routes::Delete(router, "/alerts/administration/:userId/:pillId", Routes::bind(&AlertEndpoint::removeAlertForUser, this));
    
        Routes::Get(router, "/alerts/reducedStock/:userId/:drugName", Routes::bind(&AlertEndpoint::getReducedStockAlerts, this));
        Routes::Post(router, "/alerts/reducedStock/:userId/:status", Routes::bind(&AlertEndpoint::postReducedStockAlert, this));

    }

    void getAlerts(const Rest::Request &request, Http::ResponseWriter response)
    {
        Guard guard(UserLock);
        string alerts="";
        vector<AlertManager*>::iterator i;
        
        for (i = alertManagers.begin(); i < alertManagers.end(); i++)
        {
            string temp = (*i)->AlertManager::getAdministrationAlerts();
            alerts = alerts + temp; 
            alerts = (*i)->getTextAdm();
            //temp = "";
            // temp = i->AlertManager::getExpirationAlerts();
            // alerts.insert(alerts.end(), temp.begin(), temp.end());
            // temp = "";
            // temp = i->AlertManager::getReducedStockAlerts();
            // alerts.insert(alerts.end(), temp.begin(), temp.end());
            response.send(Http::Code::Ok, "Alerts:\n" + alerts + temp);
            return;
        }
        if (alerts!= "")
        {
            response.send(Http::Code::Ok, "Alerts:\n" + alerts);
            return;
        }
        response.send(Http::Code::Not_Found, "No users\n");
    }
    void getAlertsFromUser(const Rest::Request &request, Http::ResponseWriter response)
    {
        Guard guard(UserLock);
        auto userId = request.param(":userId").as<int>();
        if(UserNamespace::checkUserExists(to_string(userId)) == false){
            response.send(Http::Code::Bad_Request, "The user doesn't exist\n");
            return;
        }
        
        AlertManager* manager = getAlertManagerOfUser(userId);
        if(manager->getUserId() == -1){
            response.send(Http::Code::Bad_Request, "No user with that id\n");
            return;
        }
        if(manager->isActive() == false){
            response.send(Http::Code::Bad_Request, "The user is not allowed to see alerts\n");
            return;
        }
        string alerts = "";
        vector<AlertManager>::iterator i;
        alerts += manager->AlertManager::getAdministrationAlerts();
        // alerts += manager.AlertManager::getExpirationAlerts();
        // alerts += manager.AlertManager::getReducedStockAlerts();
        cout<<alerts;
        if (alerts != "")
        {
            response.send(Http::Code::Ok, "Alerts:\n" + alerts);
            return;
        }
        response.send(Http::Code::Not_Found, "No alerts for user\n");
    }
    void createAlertForUser(const Rest::Request &request, Http::ResponseWriter response)
    {
        auto userId = request.param(":userId").as<int>();
        auto pillId = request.param(":pillId").as<string>();
        Guard guard(UserLock);

        if(UserNamespace::checkUserExists(to_string(userId)) == false){
            response.send(Http::Code::Bad_Request, "No user with that id\n");
            return;
        }
        AlertManager* manager = getAlertManagerOfUser(userId);
        if (manager->getUserId() == -1)
        {
            response.send(Http::Code::Not_Found, "No user with that id\n");
            return;
        }
        if(manager->isActive() == false){
            response.send(Http::Code::Bad_Request, "The user is not allowed to see alerts\n");
            return;
        }

        int status = manager->addAdmininistrationAlert(pillId);
        if (status == 0)
        {
            response.send(Http::Code::Created, "Alert added successfully\n");
            return;
        }
        else if (status == 1)
        {
            response.send(Http::Code::Internal_Server_Error, "Something went wrong\n");
            return;
        }
        else if (status == 2)
            response.send(Http::Code::Bad_Request, "There is already an alert for this drug\n");
        
    };
    void removeAlertForUser(const Rest::Request &request, Http::ResponseWriter response)
    {
        auto userId = request.param(":userId").as<int>();
        auto pillId = request.param(":pillId").as<string>();
        Guard guard(UserLock);
        
        if(UserNamespace::checkUserExists(to_string(userId)) == false){
            response.send(Http::Code::Bad_Request, "The user doesn't exist\n");
            return;
        }
        AlertManager* manager = getAlertManagerOfUser(userId);
        if (manager->getUserId() == -1)
        {
            response.send(Http::Code::Not_Found, "No user with that id\n");
            return;
        }
        if(manager->isActive() == false){
            response.send(Http::Code::Bad_Request, "The user is not allowed to see alerts\n");
            return;
        }
        int status = manager->removeAdministrationAlert(pillId);
        if (status == 0)
        {
            response.send(Http::Code::Created, "Alert removed successfully\n");
            return;
        }
        else if (status == 1)
        {
            response.send(Http::Code::Internal_Server_Error, "Something went wrong\n");
            return;
        }
        else if (status == 2)
            response.send(Http::Code::Bad_Request, "Alerts are not enabled\n");
    };
    
    void getReducedStockAlerts(const Rest::Request &request, Http::ResponseWriter response){
        Guard guard(UserLock);
        auto drugName = request.param(":drugName").as<string>();
        auto userId = request.param(":userId").as<string>();
        if(UserNamespace::checkUserExists(userId) == false){
            response.send(Http::Code::Bad_Request, "The user doesn't exist\n");
            return;
        }
        AlertManager* manager = getAlertManagerOfUser(stoi(userId));
        if(manager->getUserId() == -1){
            response.send(Http::Code::Bad_Request, "No user with that id\n");
            return;
        }
        if(manager->isActive() == false || manager->getReducedStockStatus() == false){
            response.send(Http::Code::Bad_Request, "The user is not allowed to see alerts\n");
            return;
        }
        string drugId = DrugNamespace::getSpecificDrug(drugName);
        if(drugId == ""){
            response.send(Http::Code::Bad_Request, "The drug doesn't exist\n");
            return;
        }
        string quantityString = DrugNamespace::getDrugQuantity(drugId);
        if(quantityString == ""){
            response.send(Http::Code::Ok, "There are no more pills\n");
            return;
        }
        int quantity = stoi(quantityString);
        if(quantity < 6 && quantity > 0){
            response.send(Http::Code::Ok, "The stock is low\n");
            return;
        }
        if(quantity == 0){
            response.send(Http::Code::Ok, "There are no more pills\n");
            return;
        }
        response.send(Http::Code::Ok, "There are enough pills\n");
    };
    void postReducedStockAlert(const Rest::Request &request, Http::ResponseWriter response){
        Guard guard(UserLock);
        auto userId = request.param(":userId").as<string>();
        auto status = request.param(":status").as<string>();

        if(UserNamespace::checkUserExists(userId) == false){
            response.send(Http::Code::Bad_Request, "No user with that id\n");
            return;
        }
        AlertManager *manager = getAlertManagerOfUser(stoi(userId));
        if(manager->getUserId() == -1){
            response.send(Http::Code::Bad_Request, "No user with that id\n");
            return;
        }
        if(manager->isActive() == false){
            response.send(Http::Code::Bad_Request, "The user is not allowed to see alerts\n");
            return;
        }
        if(status == "active"){
            manager->setReducedStockStatus(true);
        }
        else if(status == "inactive"){
            manager->setReducedStockStatus(false);
        }
        else{
            response.send(Http::Code::Bad_Request, "The status can be active or inactive\n");
            return;
        }
            response.send(Http::Code::Ok,"The status is changed\n");
    };
    AlertManager* getAlertManagerOfUser(int userId);
    public: 
    static int addAlertManager(int userId);
    static int deleteAlertManager(int userId);
    static AlertManager* get(int userId);
};

AlertManager* AlertEndpoint::getAlertManagerOfUser(int userId)
{
    vector<AlertManager*>::iterator i;
    for (i = alertManagers.begin(); i < alertManagers.end(); i++)
    {
        if ((*i)->getUserId() == userId)
        {
            return *i;
        }
    }
    return new AlertManager(-1, false);
}

AlertManager * AlertEndpoint::get(int userId)
{
    vector<AlertManager*>::iterator i;
    for (i = alertManagers.begin(); i < alertManagers.end(); i++)
    {
        if ((*i)->getUserId() == userId)
        {
            return *i;
        }
    }
    return new AlertManager(-1, false);
}

vector<AlertManager*> AlertEndpoint::alertManagers;
int AlertEndpoint::addAlertManager(int userId){
    try{
        AlertManager *manager = new AlertManager(userId, true);
        alertManagers.push_back(manager);
        return 0;
    }
    catch(...){
        return 1;
    }
}
int AlertEndpoint::deleteAlertManager(int userId){
    try{
        vector<AlertManager*>::iterator i;
        for(i = alertManagers.begin(); i<alertManagers.end();i++){
            if((*i)->getUserId() == userId){
                alertManagers.erase(i);
                return 0;
            } 
        }
        return 0;
        
    }
    catch(...){
        return 1;
    }
}