# Example IoT Device

## Getting Started

Git clone this project in your machine.

### Prerequisites

Build tested on Ubuntu Server. Pistache doesn't support Windows, but you can use something like [WSL](https://docs.microsoft.com/en-us/windows/wsl/install-win10) or a virtual machine with Linux.

You will need to have a C++ compiler. I used g++ that came preinstalled. Check using `g++ -v`

You will need to install the [Pistache](https://github.com/pistacheio/pistache) library.
On Ubuntu, you can install a pre-built binary as described [here](http://pistache.io/docs/#installing-pistache).

### Building

- Using Make

You can build the `cabinet` executable by running `make`.

- Manually

A step by step series of examples that tell you how to get a development env running

You should open the terminal, navigate into the root folder of this repository, and run\
`g++ main.cpp -o main -lpistache -lcrypto -lssl -lpthread`

This will compile the project using g++, into an executable called `cabinet` using the libraries `pistache`, `crypto`, `ssl`, `pthread`. You only really want pistache, but the last three are dependencies of the former.
Note that in this compilation process, the order of the libraries is important.

### Running

To start the server run\
`./main`

Your server should display the number of cores being used and no errors.

### Test

To test, open up another terminal, and type\
`curl http://localhost:9082/user/all`

You should receive something like:

1,Ionea,22,*
2,Patratica,63,-
3,Didina,58,-


Now you have the server running


## Routes

`9080: environment state:` 

- Get sensor state: `GET: environment /sensorState/:idShelf/:sensorType`


`9081: settings:` 

- `GET: /settings` -> gets device settings 

- `GET: /settings/configure` -> gets device settings and configures current date and time

- `PATCH: /settings/configure/:date/:time/:zone` -> edits device configuration (date. Time, zone)  


- `GET: /settings/shelf/:idShelf` -> gets shelf settings (pills number, temperature targeted value, humidity targeted value) 

- `GET: /settings/shelf/temperature/:idShelf `

- `GET: /settings /shelf/humidity/:idShelf`

- `PATCH: /settings/shelf/temperature/:idShelf/:value`

- `PATCH: /settings/shelf/humidity/:idShelf/:value `

 

`9082: drug:` 

- Get drugs: `GET: drug/stock`

- Get specific drug: `GET: drug/stock/:drugName`

- Get drug id: `GET: drug/getId/:drugName`

- Get drug quantity: `GET: drug/getQuantity/:drugId`

- Get expiration date: `GET: drug/getExpDate/:drugId`

- Add new drug: `POST: drug/:drugName/:quantity/:max_quantity/:max_temp/:min_temp/:humidity/:exp_date/:type`

- Retrieve drug for an user: `POST: drug/:userId/:drugName/:quantity` 



`9083: user:` 

- Get users: `GET: user/all`

- Get current user: `GET: user/current`

- Change current user: `POST/user/curent/:userId` 


- Add user: `POST: user/userName/userAge` 

- Edit user: `PATCH: user/userId/userName/userAge` 

- Remove user: `DELETE: user/:userId` 


- Get all prescriptions: `GET: user/prescription/all` 

- Get prescriptions for user: `GET: user/prescription/:userId` 

- Add medical prescription: `POST: user/prescription/:drugName/:quantity` 

- Remove medical prescription: `DELETE: user/prescription/:prescriptionId` 

- Edit medical prescription: `PATCH: user/prescription/: prescriptionId/:drugName/:quantity` 


`9084: alerts:` 

Alerta administrare medicament: 

- Add alert: `POST: alert/:userName/:details`

- Remove alert: `DELETE: alert/:userName/:details`

- Edit alert: `PATCH:  alert/:userName/:details`


Alerta stoc redus : (is reducedStock when  < 5):

- `GET: alert/reducedStock` 

- `POST: alert/reducedStock/:value (value = ON/ OFF)`


Alert produse in curs de expirare:  

- GET: `alert/expireSoon` 

- POST: `alert/ expireSoon /:value (value = ON/ OFF)`


## CSV files structe

- `user.csv` 

userId, name, age, isCurrent(*/-)

- `prescription.csv`

prescriptionId, userId, drugName, drugQuantity

- `drug.csv`

drugId, drugName, quantity, max_quantity, max_temp, min_temp, humidity, exp_date, type


## MQTT 
### Installation
----
>> sudo apt-get update\
>> sudo apt-get install mosquitto\
>> sudo apt-get install mosquitto-clients\
>> sudo apt-get install libmosquitto-dev

## Simple test from terminal
----
#### Subscribe to test topic:
>> mosquitto_sub -t "test"

#### Publish a message to test topic:
>> mosquitto_pub -m "message from mosquitto_pub client" -t "test"

## Project integration test:
----
#### Compile the publisher script (publisher-test publishes messages to /test/test1 topic):
>> g++ mqtt_pub.cpp -o mqtt_pub -lmosquitto\
>> ./mqtt_pub

#### Start Mosquitto Broker:
>> mosquitto -v 

#### Subscribe to /test/test1 topic from terminal to receive the messages:
>> mosquitto_sub -t /test/test1

### Restart the service and release the port
>> sudo service mosquitto restart\
>> sudo pkill mosquitto

---
#### Compile the subscriber script (subscriber-test receives messages from subscribed topic /test/test1):
>> g++ mqtt_sub.cpp -o mqtt_sub -lmosquitto\
>> ./mqtt_sub

#### Start Mosquitto Broker:
>> mosquitto -v 

#### Pubslish a message to /test/test1 topic from terminal:
>> mosquitto_pub -t test/test1 -m "test"

### Restart the service and release the port
>> sudo service mosquitto restart\
>> sudo pkill mosquitto


## Use MQTT to send sensor data to CabinetEndpoint
MQTT topics:
>> sensor/temperature/1\
>> sensor/temperature/2\
>> ...\
>> sensor/humidity/1\
>> sensor/humidity/2\
>> ...

### Pubslish a message to sensor/temperature/1 topic from terminal:
>> mosquitto_pub -t sensor/temperature/1 -m "25.5"

## Built With

* [Pistache](https://github.com/pistacheio/pistache) - Web server

## License

This project is licensed under the Apache 2.0 Open Source Licence - see the [LICENSE](LICENSE) file for details