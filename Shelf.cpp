#include "Environment.cpp"

class Shelf {
    public:
        Shelf() {
            ID = 0;
        }

        Shelf(int id) {
            ID = id;
        }

        ~Shelf() {}

        int getId() const {
            return this->ID;
        }

        void setId(int ID) {
            this->ID = ID;
        }

        string getSensorValue(string sensorType) {
            return environment.get(sensorType);
        }

        void setSensorValue(string sensorType, float sensorValue) {
            environment.set(sensorType, sensorValue);
        }

    private:
        int ID;
        Environment environment;
};