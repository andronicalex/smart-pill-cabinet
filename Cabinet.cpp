#include "Shelf.cpp"

class Cabinet {
    public: 
        Cabinet() {}
        Cabinet(int numShelfs, Shelf* shelfs) : numShelfs(numShelfs) {
            this->shelfs = shelfs;
        }

        ~Cabinet() {
            delete[] shelfs;
        }

        int getNumShelfs() const {
            return this->numShelfs;
        }

        void setNumShelfs(int numShelfs) {
            this->numShelfs = numShelfs;
        }

        Shelf getShelf(int idx) const {
            return shelfs[idx];
        }

        void setShelfSensorValue(int idxShelf, string sensorType, float sensorValue){
            shelfs[idxShelf].setSensorValue(sensorType, sensorValue);
            cout << idxShelf <<sensorType << sensorValue;
        }

        int set(int idxShelf, string sensorType, float sensorValue) {
            this->setShelfSensorValue(idxShelf, sensorType, sensorValue);
            Shelf currentShelf = this->getShelf(idxShelf);
            float newValue = stof(currentShelf.getSensorValue(sensorType));
            if(newValue == sensorValue) {
                return 0;
            }
            return -1;
        }

    private:
        int numShelfs;
        Shelf* shelfs;
};


// Definition of the CabinetEndpoint class
class CabinetEndpoint {
    public:
        explicit CabinetEndpoint(Address addr) : httpEndpoint(std::make_shared<Http::Endpoint>(addr)) {}

        // Initialization of the server. Additional options can be provided here
        void init(size_t thr = 2)
        {
            auto opts = Http::Endpoint::options().threads(static_cast<int>(thr));
            httpEndpoint->init(opts);
            // Server routes are loaded up
            setupRoutes();
        }

        // Server is started threaded.
        void start()
        {
            httpEndpoint->setHandler(router.handler());
            httpEndpoint->serveThreaded();
        }

        // When signaled server shuts down
        void stop()
        {
            cout << "Shutting down..." << endl;
            httpEndpoint->shutdown();
        }

        Shelf* createShelfs(int numShelfs) {
            Shelf* shelfs = (Shelf*)malloc(sizeof(Shelf) * numShelfs);
            for (int i = 0; i < numShelfs; i++) { 
                shelfs[i] = Shelf(i);
            }
            return shelfs;
        }

        // Instances of the Shelf model
        int numShelfs = 3;
        Shelf* shelfs = createShelfs(numShelfs);
        // Instance of Cabinet model
        Cabinet cabinet = Cabinet(numShelfs, shelfs);

    private:

        // Create the lock which prevents concurrent editing of the same variable 
        using Lock = std::mutex;
        using Guard = std::lock_guard<Lock>;
        Lock cabinetLock;
        
        // Defining the httpEndpoint and a router.
        std::shared_ptr<Http::Endpoint> httpEndpoint;
        Rest::Router router;

        void setupRoutes()
        {
            using namespace Rest;
            Routes::Post(router, "/environment/sensorValue/:idShelf/:sensorType/:value", Routes::bind(&CabinetEndpoint::setSettings, this));
            Routes::Get(router, "/environment/sensorValue/:idShelf/:sensorType", Routes::bind(&CabinetEndpoint::getSettings, this));
        }

        void getSettings(const Rest::Request& request, Http::ResponseWriter response) {
            auto idShelf = request.param(":idShelf").as<std::string>();
            auto sensorType = request.param(":sensorType").as<std::string>();
            // response.send(Http::Code::Ok, "Shelf ");


            Guard guard(cabinetLock);

            int numShelfs = cabinet.getNumShelfs();
            if(0 > stoi(idShelf) || stoi(idShelf) > numShelfs) {
                response.send(Http::Code::Not_Found, "Shelf " + idShelf + " was not found");
            }

            Shelf currentShelf = cabinet.getShelf(stoi(idShelf));
            string valueSetting = currentShelf.getSensorValue(sensorType);

            if (valueSetting != "") {

                // In this response I also add a couple of headers, describing the server that sent this response, and the way the content is formatted.
                using namespace Http;
                response.headers()
                    .add<Header::Server>("pistache/0.1")
                    .add<Header::ContentType>(MIME(Text, Plain));

                response.send(Http::Code::Ok, "Shelf " + idShelf + ": " + sensorType + " is " + valueSetting);
            }
            else {
                response.send(Http::Code::Not_Found, "Shelf " + idShelf + ": " + sensorType + " was not found");
            }
        }

        void setSettings(const Rest::Request& request, Http::ResponseWriter response) {
            auto idShelf = request.param(":idShelf").as<std::string>();
            auto sensorType = request.param(":sensorType").as<std::string>();
            auto sensorValue = request.param(":value").as<std::string>();

            Guard guard(cabinetLock);
            
            int numShelfs = cabinet.getNumShelfs();
            if(0 > stoi(idShelf) || stoi(idShelf) > numShelfs) {
                response.send(Http::Code::Not_Found, "Shelf " + idShelf + " was not found");
            }

            int setResponse = cabinet.set(stoi(idShelf), sensorType, stof(sensorValue));

            // Sending some confirmation or error response.
            if (setResponse == 0) {
                response.send(Http::Code::Ok, sensorType + " was set to " + sensorValue);
            }
            else {
                response.send(Http::Code::Not_Found, sensorType + " was not found and or '" + sensorValue + "' was not a valid value ");
            }
        }
};