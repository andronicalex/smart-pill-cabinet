#include "helpers.cpp"
namespace DrugNamespace
{

    vector<string> parseLine(string drug)
    {
        vector<string> drugInfo;
        stringstream tolist(drug);
        string intermediate;
        while (getline(tolist, intermediate, ','))
        {
            drugInfo.push_back(intermediate);
        }
        return drugInfo;
    }

    string getDrugId(string drugName)
    {
        string drugInfo = "";
        string currentDrugInFile = "";
        fstream fin;
        fin.open("drugs.csv", ios::in);
        string currentDrugName;
        for (std::string line; getline(fin, line);)
        {
            currentDrugInFile = line;
            if (currentDrugInFile.find(drugName) != string::npos)
            {
                vector<string> drugInfo = parseLine(currentDrugInFile);
                return drugInfo[0];
            }
        }

        fin.close();
        return "";
    }

    string getDrugQuantity(string drugId)
    {
        fstream fin;
        fin.open("drugs.csv", ios::in);
        string currentDrugInFile;
        for (std::string line; getline(fin, line);)
        {
            currentDrugInFile = line;
            if (currentDrugInFile.find(drugId) != string::npos)
            {
                vector<string> drugInfo = parseLine(currentDrugInFile);
                return drugInfo[2];
            }
        }
        fin.close();
        return "";
    }

    string getDrugExpDate(string drugId)
    {
        fstream fin;
        fin.open("drugs.csv", ios::in);
        string currentDrugInFile;
        for (std::string line; getline(fin, line);)
        {
            currentDrugInFile = line;
            if (currentDrugInFile.find(drugId) != string::npos)
            {
                vector<string> drugInfo = parseLine(currentDrugInFile);
                return drugInfo[7];
            }
        }
        fin.close();
        return "";
    }

    string getSpecificDrug(string drugName)
    {
        string drugInfo = "";
        string currentDrugInFile = "";
        fstream fin;
        fin.open("drugs.csv", ios::in);
        int drugId;
        string currentDrugName;
        for (std::string line; getline(fin, line);)
        {
            currentDrugInFile = line;
            if (currentDrugInFile.find(drugName) != string::npos)
            {
                return currentDrugInFile;
            }
        }

        fin.close();
        return "";
    }

    string existDrug(string drugId, string drugName)
    {
        string currentDrugInFile = "";
        fstream fin;
        fin.open("drugs.csv", ios::in);
        for (std::string line; getline(fin, line);)
        {
            currentDrugInFile = line;
            if (currentDrugInFile.find(drugName) != string::npos && currentDrugInFile.find(drugId) != string::npos)
            {
                return currentDrugInFile;
            }
        }

        fin.close();
        return "";
    }
    string retrieveDrug(string userId, string drugName, string quantity)
    {
        fstream fin, faux, f;
        f.open("drugs.csv", ios::in);
        faux.open("drugsAux.csv", ios::out);
        vector<string> tokens;
        string drug = getSpecificDrug(drugName);
        int index = 0;
        if (drug.find("prescriptie") != string::npos)
        {
            int index = 0;
            fin.open("prescription.csv");
            for (std::string line; getline(fin, line);)
            {
                string currentMed = line;
                if (currentMed.find(userId) != string::npos && currentMed.find(drugName) != string::npos)
                {
                    
                    stringstream check1(currentMed);
                    string intermediate;
                    while (getline(check1, intermediate, ','))
                    {
                        tokens.push_back(intermediate);
                    }

                    string pres_quantity = tokens[tokens.size() - 1];

                    int int_quantity;
                    stringstream numb(pres_quantity);
                    numb >> int_quantity;
                    vector<string> drugInfo = parseLine(drug);
                    stringstream toint(drugInfo[2]);
                    int quantityFromPr = 0;
                    toint >> quantityFromPr;

                    for (std::string line; getline(f, line);)
                    {
                        if (index)
                            faux << "\n";
                        vector<string> currentDrugInfo = parseLine(line);
                        if (stoi(currentDrugInfo[0]) == stoi(drugInfo[0]))
                        {
                            int qt = stoi(drugInfo[2]);
                            qt = qt - int_quantity;
                            
                            faux << drugInfo[0] << "," << drugInfo[1] << "," << to_string(qt) << "," << drugInfo[3] << "," << drugInfo[4] << "," << drugInfo[5] << "," << drugInfo[6] << "," << drugInfo[7]<<","<<drugInfo[8];
                        
                        }
                        else
                            faux << line;

                        index++;
                    }
                }
            }
            // removing the existing file
            remove("drugs.csv");

            // renaming the updated file with the existing file name
            rename("drugsAux.csv", "drugs.csv");
        }
        else
        {
            int index = 0;
            vector<string> drugInfo = parseLine(drug);
            string drugQuantity = drugInfo[2];
            if (stoi(drugQuantity) < stoi(quantity))
                return "";
            else
            {
                string perQuantity = drugInfo[3];
                if (stoi(quantity) > stoi(perQuantity))
                {
                    return "";
                }
                else
                {
                    int x = stoi(drugQuantity) - stoi(quantity);
                    for (std::string line; getline(f, line);)
                    {
                        if (index)
                            faux << "\n";
                        index++;
                        vector<string> currDrug = parseLine(line);
                        if (stoi(currDrug[0]) == stoi(drugInfo[0]))
                        {
                            faux << drugInfo[0] << "," << drugInfo[1] << "," << to_string(x) << "," << drugInfo[3] << "," << drugInfo[4] << "," << drugInfo[5] << "," << drugInfo[6] << "," << drugInfo[7] << "," << drugInfo[8];
                        }
                        else
                            faux << line;
                    }
                    // removing the existing file
                    remove("drugs.csv");

                    // renaming the updated file with the existing file name
                    rename("drugsAux.csv", "drugs.csv");
                }
            }
        }
        fin.close();
        faux.close();
        return "Ok";
    }

    // all drugs
    string getDrugs()
    {
        string allDrugs = "";
        fstream fin;
        fin.open("drugs.csv", ios::in);

        int index = 0;
        for (std::string line; getline(fin, line);)
        {
            if (index > 0)
                allDrugs += "\n";

            allDrugs += line;
            index++;
        }

        fin.close();
        return allDrugs;
    }

    // generate id for new drug 
    int getLastDrugId()
    {
        int lastId = 0;
        std::string line;

        fstream fin;
        fin.open("drugs.csv", ios::in);

        while (getline(fin, line))
        {
            std::stringstream ss(line);
            ss >> lastId;
        }

        fin.close();
        return lastId;
    }

    // add drug 
    bool addDrug(string name, string quantity, string max_quantity, string max_temp, string min_temp, string humidity, string exp_date, string type)
    {
        try
        {
            fstream f;
            f.open("drugs.csv", ios::out | ios::app);
            int drugId = getLastDrugId();
            if (!drugId)
                f << drugId + 1 << "," << name << "," << quantity << "," << max_quantity << "," << max_temp << "," << min_temp << "," << humidity << "," << exp_date << "," << type;
            else

                f << "\n"
                  << drugId + 1 << "," << name << "," << quantity << "," << max_quantity << "," << max_temp << "," << min_temp << "," << humidity << "," << exp_date << "," << type;

            f.close();
            // adaugarea in fisier s-a realizat cu succes
            return 1;
        }
        catch (...)
        {
            return 0;
        }
    }

}