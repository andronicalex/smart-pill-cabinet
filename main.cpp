#include "helpers.cpp"
#include "userEndpoint.cpp"
#include "drugEndpoint.cpp"

#include "settingsEndpoint.cpp"
#include "Cabinet.cpp"

const int portCABINET = 9080;
const int portSETTINGS = 9081;
const int portDRUG = 9082;
const int portUSER = 9083;
const int portALERT = 9084;

Port cabinetPort(portCABINET);
Address cabinetAddr(Ipv4::any(), cabinetPort);
CabinetEndpoint cabinetStats(cabinetAddr);

// Number of threads used by the server
const int THREADS = 2;

struct mosquitto *mosq;

void on_connect(struct mosquitto *mosq, void *obj, int rc) {
    cout << "Client ID: " << *(int *) obj << "\n";
    if (rc) {
        cout << "Error rc=" << rc << "\n";
        exit(-1);
    }

    mosquitto_subscribe(mosq, NULL, "sensor/temperature/0", 0);
    mosquitto_subscribe(mosq, NULL, "sensor/temperature/1", 0);
    mosquitto_subscribe(mosq, NULL, "sensor/temperature/2", 0);
    mosquitto_subscribe(mosq, NULL, "sensor/humidity/0", 0);
    mosquitto_subscribe(mosq, NULL, "sensor/humidity/1", 0);
    mosquitto_subscribe(mosq, NULL, "sensor/humidity/2", 0);
}

void on_message(struct mosquitto *mosq, void *obj, const struct mosquitto_message *msg) {
    cout << "New message with topic " << msg->topic << ": " << (char*) msg->payload << "\n";
    string topicstr ((char*) msg->topic);
    string delimiter = "/";
    string path1 = topicstr.substr(topicstr.find(delimiter) + 1, topicstr.length());
    string sensorType = path1.substr(0, path1.find(delimiter));
    string shelfId = path1.substr(path1.find(delimiter) + 1, path1.length());

    cabinetStats.cabinet.setShelfSensorValue(stoi(shelfId), sensorType, atoi((char*) msg->payload));
}

int mqtt() 
{
    int rc, client_id = 12;
    const int default_mqqt_port = 1883;

    mosquitto_lib_init();

    mosq = mosquitto_new("subscriber-test", true, &client_id);
    mosquitto_connect_callback_set(mosq, on_connect);
    mosquitto_message_callback_set(mosq, on_message);

    rc = mosquitto_connect(mosq, "localhost", default_mqqt_port, 60);

    if (rc) {
        cout << "Client connection error!\n";
        mosquitto_destroy(mosq);
        return -1;
    }

    cout << "Client successfully connected to the broker!\n";
    mosquitto_loop_start(mosq);
    cout << "Press ENTER to quit...\n";
    getchar();

    mosquitto_loop_stop(mosq, true);    
    mosquitto_disconnect(mosq);
    mosquitto_destroy(mosq);
    mosquitto_lib_cleanup();
    return 0;
}


int main(int argc, char *argv[])
{
    // This code is needed for gracefull shutdown of the server when no longer needed.
    sigset_t signals;
    if (sigemptyset(&signals) != 0 || sigaddset(&signals, SIGTERM) != 0 || sigaddset(&signals, SIGINT) != 0 || sigaddset(&signals, SIGHUP) != 0 || pthread_sigmask(SIG_BLOCK, &signals, nullptr) != 0)
    {
        perror("install signal handler failed");
        return 1;
    }

    // ................................................................................................

    // Set a port on which your server to communicate
    Port userPort(portUSER);
    Port drugPort(portDRUG);
    Port alertPort(portALERT);
    Port settingsPort(portSETTINGS);

    Address userAddr(Ipv4::any(), userPort);
    Address drugAddr(Ipv4::any(), drugPort);
    Address alertAddr(Ipv4::any(), alertPort);
    Address settingsAddr(Ipv4::any(), settingsPort);

    cout << "Cores = " << hardware_concurrency() << endl;
    cout << "Using " << THREADS << " threads" << endl;

    // Instance of the class that defines what the server can do.
    UserEndpoint userStats(userAddr);
    DrugEndpoint drugStats(drugAddr);
    AlertEndpoint alertStats(alertAddr);
    SettingsEndpoint settingsStats(settingsAddr);

    // Initialize and start the server
    userStats.init(THREADS);
    drugStats.init(THREADS);
    alertStats.init(THREADS);
    settingsStats.init(THREADS);
    cabinetStats.init(THREADS);

    userStats.start();
    drugStats.start();
    alertStats.start();
    settingsStats.start();
    cabinetStats.start();

    
    int errorValue = mqtt();
    if(errorValue == -1) {
        cout << "Connection lost";
        return -1;
    } else {
        cout << "merge";
    }

    // ................................................................................................

    // Code that waits for the shutdown sinal for the server
    int signal = 0;
    int status = sigwait(&signals, &signal);
    if (status == 0)
    {
        std::cout << "received signal " << signal << std::endl;
    }
    else
    {
        std::cerr << "sigwait returns " << status << std::endl;
    }

    // ................................................................................................

    userStats.stop();
    drugStats.stop();
    alertStats.stop();
    settingsStats.stop();
    cabinetStats.stop();
}