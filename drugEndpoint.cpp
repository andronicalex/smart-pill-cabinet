#include "helpers.cpp"


// Definition of the CabinetEndpoint class
class DrugEndpoint
{
public:
    explicit DrugEndpoint(Address addr) : httpEndpoint(std::make_shared<Http::Endpoint>(addr)) {}

    // Initialization of the server. Additional options can be provided here
    void init(size_t thr = 2)
    {
        auto opts = Http::Endpoint::options().threads(static_cast<int>(thr));
        httpEndpoint->init(opts);
        // Server routes are loaded up
        setupRoutes();
    }

    // Server is started threaded.
    void start()
    {
        httpEndpoint->setHandler(router.handler());
        httpEndpoint->serveThreaded();
    }

    // When signaled server shuts down
    void stop()
    {
        cout << "Shutting down..." << endl;
        httpEndpoint->shutdown();
    }

private:
    // Create the lock which prevents concurrent editing of the same variable
    using Lock = std::mutex;
    using Guard = std::lock_guard<Lock>;
    Lock UserLock;

    // Defining the httpEndpoint and a router.
    std::shared_ptr<Http::Endpoint> httpEndpoint;
    Rest::Router router;

    void setupRoutes()
    {
        using namespace Rest;
        Routes::Get(router, "/drug/stock", Routes::bind(&DrugEndpoint::getDrugs, this));
        Routes::Get(router, "/drug/stock/:drugName", Routes::bind(&DrugEndpoint::getSpecificDrug, this));
        Routes::Get(router, "/drug/getId/:drugName", Routes::bind(&DrugEndpoint::getDrugId, this));
        Routes::Get(router, "/drug/getQuantity/:drugId", Routes::bind(&DrugEndpoint::getQuantity, this));
        Routes::Get(router, "/drug/getExpDate/:drugId", Routes::bind(&DrugEndpoint::getExpDate, this));
        Routes::Get(router, "/drug/existDrug/:drugId/:drugName", Routes::bind(&DrugEndpoint::existDrug, this));
        // add drug
        Routes::Post(router, "/drug/:drugName/:quantity/:max_quantity/:max_temp/:min_temp/:humidity/:exp_date/:type", Routes::bind(&DrugEndpoint::addDrug, this));
        Routes::Post(router, "/drug/:userId/:drugName/:quantity", Routes::bind(&DrugEndpoint::retrieveDrug, this));
    }

    void getDrugs(const Rest::Request &request, Http::ResponseWriter response)
    {
        Guard guard(UserLock);

        string valueSetting = DrugNamespace::getDrugs();

        if (valueSetting != "")
        {
            // In this response I also add a couple of headers, describing the server that sent this response, and the way the content is formatted.
            using namespace Http;
            response.headers()
                .add<Header::Server>("pistache/0.1")
                .add<Header::ContentType>(MIME(Text, Plain));

            response.send(Http::Code::Ok, "Drugs:\n" + valueSetting);
        }
        else
        {
            response.send(Http::Code::Not_Found, "No meds yet.\n");
        }
    }

    void retrieveDrug(const Rest::Request &request, Http::ResponseWriter response)
    {
        Guard guard(UserLock);
        auto userId = request.param(":userId").as<std::string>();
        auto drugName = request.param(":drugName").as<std::string>();
        auto quantity = request.param(":quantity").as<std::string>();

        string valueSetting = DrugNamespace::retrieveDrug(userId, drugName, quantity);

        if (valueSetting != "")
        {
            // In this response I also add a couple of headers, describing the server that sent this response, and the way the content is formatted.
            using namespace Http;
            response.headers()
                .add<Header::Server>("pistache/0.1")
                .add<Header::ContentType>(MIME(Text, Plain));

            response.send(Http::Code::Ok, valueSetting);
        }
        else
        {
            response.send(Http::Code::Not_Found, "Error");
        }
    }

    void existDrug(const Rest::Request &request, Http::ResponseWriter response)
    {
        Guard guard(UserLock);
        auto drugId = request.param(":drugId").as<std::string>();
        auto drugName = request.param(":drugName").as<std::string>();
        string valueSetting = DrugNamespace::existDrug(drugId, drugName);

        if (valueSetting != "")
        {
            // In this response I also add a couple of headers, describing the server that sent this response, and the way the content is formatted.
            using namespace Http;
            response.headers()
                .add<Header::Server>("pistache/0.1")
                .add<Header::ContentType>(MIME(Text, Plain));

            response.send(Http::Code::Ok, "Drug:\n" + valueSetting);
        }
        else
        {
            response.send(Http::Code::Not_Found, "This drug doesn't exist.\n");
        }
    }

    void getDrugId(const Rest::Request &request, Http::ResponseWriter response)
    {
        Guard guard(UserLock);
        auto name = request.param(":drugName").as<std::string>();
        string valueSetting = DrugNamespace::getDrugId(name);

        if (valueSetting != "")
        {
            // In this response I also add a couple of headers, describing the server that sent this response, and the way the content is formatted.
            using namespace Http;
            response.headers()
                .add<Header::Server>("pistache/0.1")
                .add<Header::ContentType>(MIME(Text, Plain));

            response.send(Http::Code::Ok, "Drug Id:\n" + valueSetting);
        }
        else
        {
            response.send(Http::Code::Not_Found, "This drug doesn't exist.\n");
        }
    }

    void getQuantity(const Rest::Request &request, Http::ResponseWriter response)
    {
        Guard guard(UserLock);
        auto drugId = request.param(":drugId").as<std::string>();
        string valueSetting = DrugNamespace::getDrugQuantity(drugId);

        if (valueSetting != "")
        {
            // In this response I also add a couple of headers, describing the server that sent this response, and the way the content is formatted.
            using namespace Http;
            response.headers()
                .add<Header::Server>("pistache/0.1")
                .add<Header::ContentType>(MIME(Text, Plain));

            response.send(Http::Code::Ok, "Drug Quantity:\n" + valueSetting);
        }
        else
        {
            response.send(Http::Code::Not_Found, "This drug doesn't exist.\n");
        }
    }

    void getExpDate(const Rest::Request &request, Http::ResponseWriter response)
    {
        Guard guard(UserLock);
        auto drugId = request.param(":drugId").as<std::string>();
        string valueSetting = DrugNamespace::getDrugExpDate(drugId);

        if (valueSetting != "")
        {
            // In this response I also add a couple of headers, describing the server that sent this response, and the way the content is formatted.
            using namespace Http;
            response.headers()
                .add<Header::Server>("pistache/0.1")
                .add<Header::ContentType>(MIME(Text, Plain));

            response.send(Http::Code::Ok, "Drug exp date:\n" + valueSetting);
        }
        else
        {
            response.send(Http::Code::Not_Found, "This drug doesn't exist.\n");
        }
    }
    void getSpecificDrug(const Rest::Request &request, Http::ResponseWriter response)
    {
        Guard guard(UserLock);
        auto name = request.param(":drugName").as<std::string>();
        string valueSetting = DrugNamespace::getSpecificDrug(name);

        if (valueSetting != "")
        {
            // In this response I also add a couple of headers, describing the server that sent this response, and the way the content is formatted.
            using namespace Http;
            response.headers()
                .add<Header::Server>("pistache/0.1")
                .add<Header::ContentType>(MIME(Text, Plain));

            response.send(Http::Code::Ok, "Drug:\n" + valueSetting);
        }
        else
        {
            response.send(Http::Code::Not_Found, "No users yet.\n");
        }
    }

    void addDrug(const Rest::Request &request, Http::ResponseWriter response)
    {
        auto name = request.param(":drugName").as<std::string>();
        auto quantity = request.param(":quantity").as<std::string>();
        auto max_quantity = request.param(":max_quantity").as<std::string>();
        auto max_temp = request.param(":max_temp").as<std::string>();
        auto min_temp = request.param(":min_temp").as<std::string>();
        auto humidity = request.param(":humidity").as<std::string>();
        auto exp_date = request.param(":exp_date").as<std::string>();
        auto type = request.param(":type").as<std::string>();

        Guard guard(UserLock);

        int setResponse = DrugNamespace::addDrug(name, quantity, max_quantity, max_temp, min_temp, humidity, exp_date, type);

        if (setResponse == 1)
        {
            response.send(Http::Code::Ok, "Drug added successfully!\n");
        }
        else
        {
            response.send(Http::Code::Not_Found, "Failed to create drug!\n");
        }
    }
};