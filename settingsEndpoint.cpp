#include "helpers.cpp"

namespace SettingsNamespace
{
    struct ShelfSettings
    {
        string status;
        string pillsNumber;
        string temperatureTargetedValue;
        string humidityTargetedValue;
    };

    string getSettings()
    {
        string result = "";

        fstream fin;
        fin.open("settings.csv", ios::in);

        for (std::string line; getline(fin, line);)
        {
            line += "\n";
            result = result + line;
        }

        fin.close();
        return result;
    }

    string getSettingsConfigure()
    {
        time_t now = time(0);
        tm *ltm = localtime(&now);

        string result = "";

        fstream fin, faux;
        fin.open("settings.csv", ios::in);
        faux.open("settings_aux.csv", ios::out);

        std::string delimiter = ",";
        std::string line;
        std::string key;

        // set current date
        std::string date = to_string(1900 + ltm->tm_year) + "." + to_string(1 + ltm->tm_mon) + "." + to_string(ltm->tm_mday);
        getline(fin, line);
        key = line.substr(0, line.find(delimiter));
        result += key + "," + date + "\n";
        faux << key + "," + date + "\n";

        // set current time
        std::string time = to_string(5 + ltm->tm_hour) + ":" + to_string(30 + ltm->tm_min) + ":" + to_string(ltm->tm_sec);
        getline(fin, line);
        key = line.substr(0, line.find(delimiter));
        result += key + "," + time + "\n";
        faux << key + "," + time + "\n";

        // set unknown zone -
        std::string zone = "-";
        getline(fin, line);
        key = line.substr(0, line.find(delimiter));
        result += key + "," + zone + "\n";
        faux << key + "," + zone + "\n";

        for (std::string line; getline(fin, line);)
        {
            line += "\n";
            result = result + line;
            faux << line;
        }

        fin.close();
        faux.close();

        // removing the existing file
        remove("settings.csv");

        // renaming the updated file with the existing file name
        rename("settings_aux.csv", "settings.csv");

        return result;
    }

    ShelfSettings getSettingsShelf(string id)
    {
        fstream fin;
        fin.open("settings.csv", ios::in);

        std::string delimiter = ",";
        std::string key;
        std::string value;
        std::int8_t noOfShelves;

        for (std::string line; getline(fin, line);)
        {
            key = line.substr(0, line.find(delimiter));
            value = line.substr(line.find(delimiter) + 1, line.length());

            if (key.compare("number of shelves") == 0)
            {
                stringstream ss;
                ss << value;
                ss >> noOfShelves;
                break;
            }
        }

        std::string line;
        std::string shelfId;
        struct ShelfSettings shelfSettings;
        bool foundShelf = false;

        for (std::int8_t index = 1; index <= noOfShelves; index++)
        {
            getline(fin, line);
            shelfId = line.substr(0, line.find(delimiter));
            line = line.substr(line.find(delimiter) + 1, line.length());
            if (shelfId.compare(id) == 0)
            {
                shelfSettings.status = "OK";
                shelfSettings.pillsNumber = line.substr(0, line.find(delimiter));
                line = line.substr(line.find(delimiter) + 1, line.length());
                shelfSettings.temperatureTargetedValue = line.substr(0, line.find(delimiter));
                line = line.substr(line.find(delimiter) + 1, line.length());
                shelfSettings.humidityTargetedValue = line.substr(0, line.find(delimiter));
                foundShelf = true;
                break;
            }
        }

        fin.close();

        if (!foundShelf)
        {
            shelfSettings.status = "Non-existent shelf";
            shelfSettings.pillsNumber = "-";
            shelfSettings.temperatureTargetedValue = "-";
            shelfSettings.humidityTargetedValue = "-";
        }

        return shelfSettings;
    }

    string getSettingsShelfTemperature(string id)
    {
        fstream fin;
        fin.open("settings.csv", ios::in);

        std::string delimiter = ",";
        std::string key;
        std::string value;
        std::int8_t noOfShelves;

        for (std::string line; getline(fin, line);)
        {
            key = line.substr(0, line.find(delimiter));
            value = line.substr(line.find(delimiter) + 1, line.length());

            if (key.compare("number of shelves") == 0)
            {
                stringstream ss;
                ss << value;
                ss >> noOfShelves;
                break;
            }
        }

        std::string line;
        std::string shelfId;

        for (std::int8_t index = 1; index <= noOfShelves; index++)
        {
            getline(fin, line);
            shelfId = line.substr(0, line.find(delimiter));
            if (shelfId.compare(id) == 0)
            {
                line = line.substr(line.find(delimiter) + 1, line.length());
                line = line.substr(line.find(delimiter) + 1, line.length());
                fin.close();
                return line.substr(0, line.find(delimiter));
            }
        }

        fin.close();
        return "Non existent shelf";
    }

    string getSettingsShelfHumidity(string id)
    {
        fstream fin;
        fin.open("settings.csv", ios::in);

        std::string delimiter = ",";
        std::string key;
        std::string value;
        std::int8_t noOfShelves;

        for (std::string line; getline(fin, line);)
        {
            key = line.substr(0, line.find(delimiter));
            value = line.substr(line.find(delimiter) + 1, line.length());

            if (key.compare("number of shelves") == 0)
            {
                stringstream ss;
                ss << value;
                ss >> noOfShelves;
                break;
            }
        }

        std::string line;
        std::string shelfId;

        for (std::int8_t index = 1; index <= noOfShelves; index++)
        {
            getline(fin, line);
            shelfId = line.substr(0, line.find(delimiter));
            if (shelfId.compare(id) == 0)
            {
                line = line.substr(line.find(delimiter) + 1, line.length());
                line = line.substr(line.find(delimiter) + 1, line.length());
                line = line.substr(line.find(delimiter) + 1, line.length());
                fin.close();
                return line.substr(0, line.find(delimiter));
            }
        }

        fin.close();
        return "Non existent shelf";
    }

    bool editSettingsConfigure(string date, string time, string zone)
    {
        try
        {
            fstream fin, faux;
            fin.open("settings.csv", ios::in);
            faux.open("settings_aux.csv", ios::out);

            std::string delimiter = ",";
            std::string line;
            std::string key;

            // set date
            getline(fin, line);
            key = line.substr(0, line.find(delimiter));
            faux << key + "," + date + "\n";

            // set time
            getline(fin, line);
            key = line.substr(0, line.find(delimiter));
            faux << key + "," + time + "\n";

            // set zone
            getline(fin, line);
            key = line.substr(0, line.find(delimiter));
            faux << key + "," + zone + "\n";

            for (std::string line; getline(fin, line);)
            {
                line += "\n";
                faux << line;
            }

            fin.close();
            faux.close();

            // removing the existing file
            remove("settings.csv");

            // renaming the updated file with the existing file name
            rename("settings_aux.csv", "settings.csv");

            return 1;
        }
        catch (...)
        {
            return 0;
        }
    }

    int editSettingsShelfTemperature(string id, string temperature)
    {
        try
        {
            fstream fin, faux;
            fin.open("settings.csv", ios::in);
            faux.open("settings_aux.csv", ios::out);

            std::string delimiter = ",";
            std::string key;
            std::string value;
            std::int8_t noOfShelves;

            for (std::string line; getline(fin, line);)
            {
                faux << line << "\n";
                key = line.substr(0, line.find(delimiter));
                value = line.substr(line.find(delimiter) + 1, line.length());

                if (key.compare("number of shelves") == 0)
                {
                    stringstream ss;
                    ss << value;
                    ss >> noOfShelves;
                    break;
                }
            }

            std::string line;
            std::string shelfId;
            struct ShelfSettings shelfSettings;
            bool foundShelf = false;

            for (std::int8_t index = 1; index <= noOfShelves; index++)
            {
                getline(fin, line);
                shelfId = line.substr(0, line.find(delimiter));
                if (shelfId.compare(id) == 0)
                {
                    shelfSettings.status = "OK";
                    line = line.substr(line.find(delimiter) + 1, line.length());
                    shelfSettings.pillsNumber = line.substr(0, line.find(delimiter));
                    line = line.substr(line.find(delimiter) + 1, line.length());
                    shelfSettings.temperatureTargetedValue = line.substr(0, line.find(delimiter));
                    line = line.substr(line.find(delimiter) + 1, line.length());
                    shelfSettings.humidityTargetedValue = line.substr(0, line.find(delimiter));
                    faux << id + "," + shelfSettings.pillsNumber + "," + temperature + "," + shelfSettings.humidityTargetedValue + "\n";
                    foundShelf = true;
                }
                else
                {
                    faux << line << "\n";
                }
            }

            fin.close();
            faux.close();

            // removing the existing file
            remove("settings.csv");

            // renaming the updated file with the existing file name
            rename("settings_aux.csv", "settings.csv");

            if (!foundShelf)
            {
                return -1;
            }

            return 1;
        }
        catch (...)
        {
            return 0;
        }
    }

    int editSettingsShelfHumidity(string id, string humidity)
    {
        try
        {
            fstream fin, faux;
            fin.open("settings.csv", ios::in);
            faux.open("settings_aux.csv", ios::out);

            std::string delimiter = ",";
            std::string key;
            std::string value;
            std::int8_t noOfShelves;

            for (std::string line; getline(fin, line);)
            {
                faux << line << "\n";
                key = line.substr(0, line.find(delimiter));
                value = line.substr(line.find(delimiter) + 1, line.length());

                if (key.compare("number of shelves") == 0)
                {
                    stringstream ss;
                    ss << value;
                    ss >> noOfShelves;
                    break;
                }
            }

            std::string line;
            std::string shelfId;
            struct ShelfSettings shelfSettings;
            bool foundShelf = false;

            for (std::int8_t index = 1; index <= noOfShelves; index++)
            {
                getline(fin, line);
                shelfId = line.substr(0, line.find(delimiter));
                if (shelfId.compare(id) == 0)
                {
                    shelfSettings.status = "OK";
                    line = line.substr(line.find(delimiter) + 1, line.length());
                    shelfSettings.pillsNumber = line.substr(0, line.find(delimiter));
                    line = line.substr(line.find(delimiter) + 1, line.length());
                    shelfSettings.temperatureTargetedValue = line.substr(0, line.find(delimiter));
                    line = line.substr(line.find(delimiter) + 1, line.length());
                    shelfSettings.humidityTargetedValue = line.substr(0, line.find(delimiter));
                    faux << id + "," + shelfSettings.pillsNumber + "," + shelfSettings.temperatureTargetedValue + "," + humidity + "\n";
                    foundShelf = true;
                }
                else
                {
                    faux << line << "\n";
                }
            }

            fin.close();
            faux.close();

            // removing the existing file
            remove("settings.csv");

            // renaming the updated file with the existing file name
            rename("settings_aux.csv", "settings.csv");

            if (!foundShelf)
            {
                return -1;
            }

            return 1;
        }
        catch (...)
        {
            return 0;
        }
    }
}

class SettingsEndpoint
{
public:
    explicit SettingsEndpoint(Address addr) : httpEndpoint(std::make_shared<Http::Endpoint>(addr)) {}

    // Initialization of the server. Additional options can be provided here
    void init(size_t thr = 2)
    {
        auto opts = Http::Endpoint::options().threads(static_cast<int>(thr));
        httpEndpoint->init(opts);
        // Server routes are loaded up
        setupRoutes();
    }

    // Server is started threaded.
    void start()
    {
        httpEndpoint->setHandler(router.handler());
        httpEndpoint->serveThreaded();
    }

    // When signaled server shuts down
    void stop()
    {
        cout << "Shutting down..." << endl;
        httpEndpoint->shutdown();
    }

private:
    // Create the lock which prevents concurrent editing of the same variable
    using Lock = std::mutex;
    using Guard = std::lock_guard<Lock>;
    Lock UserLock;

    // Defining the httpEndpoint and a router.
    std::shared_ptr<Http::Endpoint> httpEndpoint;
    Rest::Router router;

    void setupRoutes()
    {
        using namespace Rest;
        Routes::Get(router, "/settings", Routes::bind(&SettingsEndpoint::getSettings, this));
        Routes::Get(router, "/settings/configure", Routes::bind(&SettingsEndpoint::getSettingsConfigure, this));
        Routes::Get(router, "/settings/shelf/:idShelf", Routes::bind(&SettingsEndpoint::getSettingsShelf, this));
        Routes::Get(router, "/settings/shelf/temperature/:idShelf", Routes::bind(&SettingsEndpoint::getSettingsShelfTemperature, this));
        Routes::Get(router, "/settings/shelf/humidity/:idShelf", Routes::bind(&SettingsEndpoint::getSettingsShelfHumidity, this));

        Routes::Patch(router, "/settings/configure/:date/:time/:zone", Routes::bind(&SettingsEndpoint::editSettingsConfigure, this));
        Routes::Patch(router, "/settings/shelf/temperature/:idShelf/:value", Routes::bind(&SettingsEndpoint::editSettingsShelfTemperature, this));
        Routes::Patch(router, "/settings/shelf/humidity/:idShelf/:value", Routes::bind(&SettingsEndpoint::editSettingsShelfHumidity, this));
    }

    void getSettings(const Rest::Request &request, Http::ResponseWriter response)
    {
        Guard guard(UserLock);

        string valueSetting = SettingsNamespace::getSettings();

        using namespace Http;
        response.headers()
            .add<Header::Server>("pistache/0.1")
            .add<Header::ContentType>(MIME(Text, Plain));

        response.send(Http::Code::Ok, "Settings:\n" + valueSetting);
    }

    void getSettingsConfigure(const Rest::Request &request, Http::ResponseWriter response)
    {
        Guard guard(UserLock);

        string valueSetting = SettingsNamespace::getSettingsConfigure();

        using namespace Http;
        response.headers()
            .add<Header::Server>("pistache/0.1")
            .add<Header::ContentType>(MIME(Text, Plain));

        response.send(Http::Code::Ok, "Settings:\n" + valueSetting);
    }

    void getSettingsShelf(const Rest::Request &request, Http::ResponseWriter response)
    {
        string id = request.param(":idShelf").as<std::string>();

        Guard guard(UserLock);

        SettingsNamespace::ShelfSettings shelfSettings = SettingsNamespace::getSettingsShelf(id);

        if (shelfSettings.status.compare("OK") == 0)
        {
            string valueSetting = "Shelf id: " + id + "\n" + "Pills number: " + shelfSettings.pillsNumber + "\n" + "Temperature targeted value: " + shelfSettings.temperatureTargetedValue + "\n" + "Humidity targeted value: " + shelfSettings.humidityTargetedValue;

            using namespace Http;
            response.headers()
                .add<Header::Server>("pistache/0.1")
                .add<Header::ContentType>(MIME(Text, Plain));

            response.send(Http::Code::Ok, "Shelf settings:\n" + valueSetting);
        }
        else
        {
            response.send(Http::Code::Not_Found, "Non-existent shelf\n");
        }
    }

    void getSettingsShelfTemperature(const Rest::Request &request, Http::ResponseWriter response)
    {
        string id = request.param(":idShelf").as<std::string>();

        Guard guard(UserLock);

        string valueSetting = SettingsNamespace::getSettingsShelfTemperature(id);

        if (valueSetting.compare("Non existent shelf") != 0)
        {
            using namespace Http;
            response.headers()
                .add<Header::Server>("pistache/0.1")
                .add<Header::ContentType>(MIME(Text, Plain));

            response.send(Http::Code::Ok, "Shelf targeted temperature:\n" + valueSetting);
        }
        else
        {
            response.send(Http::Code::Not_Found, "Non-existent shelf\n");
        }
    }

    void getSettingsShelfHumidity(const Rest::Request &request, Http::ResponseWriter response)
    {
        string id = request.param(":idShelf").as<std::string>();

        Guard guard(UserLock);

        string valueSetting = SettingsNamespace::getSettingsShelfHumidity(id);

        if (valueSetting.compare("Non existent shelf") != 0)
        {
            using namespace Http;
            response.headers()
                .add<Header::Server>("pistache/0.1")
                .add<Header::ContentType>(MIME(Text, Plain));

            response.send(Http::Code::Ok, "Shelf targeted humidity:\n" + valueSetting);
        }
        else
        {
            response.send(Http::Code::Not_Found, "Non-existent shelf\n");
        }
    }

    void editSettingsConfigure(const Rest::Request &request, Http::ResponseWriter response)
    {
        auto date = request.param(":date").as<std::string>();
        auto time = request.param(":time").as<std::string>();
        auto zone = request.param(":zone").as<std::string>();

        Guard guard(UserLock);

        bool setResponse = SettingsNamespace::editSettingsConfigure(date, time, zone);

        if (setResponse == 1)
        {
            response.send(Http::Code::Ok, "Device settings edited successfully!\n");
        }
        else
        {
            response.send(Http::Code::Not_Found, "Failed to edit device settings!\n");
        }
    }

    void editSettingsShelfTemperature(const Rest::Request &request, Http::ResponseWriter response)
    {
        auto idShelf = request.param(":idShelf").as<std::string>();
        auto value = request.param(":value").as<std::string>();

        Guard guard(UserLock);

        int setResponse = SettingsNamespace::editSettingsShelfTemperature(idShelf, value);

        if (setResponse == 1)
        {
            response.send(Http::Code::Ok, "Shelf targeted temperature edited successfully!\n");
        }
        else if (setResponse == -1)
        {
            response.send(Http::Code::Not_Found, "Non-existent shelf!\n");
        }
        else
        {
            response.send(Http::Code::Not_Found, "Failed to edit device settings!\n");
        }
    }

    void editSettingsShelfHumidity(const Rest::Request &request, Http::ResponseWriter response)
    {
        auto idShelf = request.param(":idShelf").as<std::string>();
        auto value = request.param(":value").as<std::string>();

        Guard guard(UserLock);

        int setResponse = SettingsNamespace::editSettingsShelfHumidity(idShelf, value);

        if (setResponse == 1)
        {
            response.send(Http::Code::Ok, "Shelf targeted humidity edited successfully!\n");
        }
        else if (setResponse == -1)
        {
            response.send(Http::Code::Not_Found, "Non-existent shelf!\n");
        }
        else
        {
            response.send(Http::Code::Not_Found, "Failed to edit device settings!\n");
        }
    }
};
