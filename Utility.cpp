#include "helpers.cpp"

using namespace std;
using namespace Pistache;

vector<string> readRecord(string filename)
{
    fstream fin;
    fin.open(filename, ios::in);

    vector<string> row;
    string temp, record, data;

    if(fin >> temp)
    {
        row.clear();

        getline(fin, record);
        stringstream s(record);
        
        while(getline(s, data, ',')) {
            row.push_back(data);
        }
    }
    else{
        fin.close();
    }

    return row;
}